# [OSMIUM] OSMIUM Receiver Service
---
*Receiver* service untuk OSMIUM yang menerima request dari pengguna yang akan melakukan kompresi

## Endpoint
1. **[CREATE DATA]** POST   /create/
	* Function: Memvalidasi token dan mengirim data untuk dicompress
    * Content-type:
        * json
    * Request Body:
        | Key       	| Type  			|
        | :---:     	| :-:   			|
        | access_token   		| String   			|
		| links   	| List		   	|

## Deployment
* URL: [osmium-receiver](https://osmium-receiver.herokuapp.com/)
* DOCS: [osmium-receiver docs](https://osmium-receiver.herokuapp.com/api/docs/)
	