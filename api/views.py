from django.views.decorators.csrf import csrf_exempt
import requests
from rest_framework import status
from rest_framework.decorators import api_view, schema
from rest_framework.response import Response

import coreapi
from rest_framework.schemas import AutoSchema

# Message queue
import pika
import json

import logging
from logstash_async.handler import AsynchronousLogstashHandler
from logstash_async.handler import LogstashFormatter

from django.views.decorators.cache import cache_page

receiver_schema = AutoSchema(manual_fields=[
	coreapi.Field("links", required=True, location="form", type="array", description="links of gdrive files here"),
])

# Create the logger and set it's logging level
logger = logging.getLogger("logstash")
logger.setLevel(logging.INFO)        

# Create the handler
handler = AsynchronousLogstashHandler(
    host='elk.faishol.net', 
    port=5000, 
    ssl_enable=False, 
    ssl_verify=False,
    database_path='')
# Here you can specify additional formatting on your log record/message
formatter = LogstashFormatter()
handler.setFormatter(formatter)

# Assign handler to the logger
logger.addHandler(handler)

def responseCreator(status = status.HTTP_200_OK, data = None, error = None):
    return Response({
					"data": data,
					"error": error,
				}, status=status)

def verify_token(access_token):
	host_local = 'http://127.0.0.1:8000/users/verify-token/'
	host_remote = 'https://osmium-auth.herokuapp.com/users/verify-token/'
	r = requests.get(host_remote + access_token + '/')
	if r.status_code == 200:
		return r.json()
	else:
		return None

def get_token(header):
	return header['HTTP_AUTHORIZATION']

@api_view(['POST'])
@csrf_exempt
@schema(receiver_schema)
def receiver(request):
	try:
		access_token = get_token(request.META)
		
		if (access_token == None):
			logger.error("[RECEIVER]: Token None")
			return responseCreator(error="Request not valid",status=status.HTTP_403_FORBIDDEN)

		access_token = access_token.split(" ")[1]
		links = request.data.get('files')

		data = verify_token(access_token)

		if data is not None:
			message = {
				"username": data["data"]["user"], 
				"data": links,
				"query_type": "create"
			}
			# Message queue
			credentials = pika.PlainCredentials('osmium', 'osmium12345678')
			connection = pika.BlockingConnection(
				pika.ConnectionParameters(host='rabbitmq.faishol.net', credentials=credentials)
			)
			channel = connection.channel()
			channel.queue_declare(queue='servicemanager_queue')
			if not connection or connection.is_closed:
				connection = pika.BlockingConnection(
					pika.ConnectionParameters(host='rabbitmq.faishol.net', credentials=credentials)
				)
				channel = connection.channel()
				channel.queue_declare(queue='servicemanager_queue')
			channel.basic_publish(exchange='', routing_key='servicemanager_queue', body=json.dumps(message))
			print("Queue sent!")
			connection.close()
			logger.info("[RECEIVER]: Create for " +  data["data"]["user"] + " with " + str(len(links)) + " links")
			return responseCreator()
		else:
			logger.error("[RECEIVER]: Token not valid")
			return responseCreator(error="Request not valid",status=status.HTTP_403_FORBIDDEN)
	except Exception as e:
		logger.error("[RECEIVER]: Request not valid " + str(e))
		return responseCreator(error="Request not valid",status=status.HTTP_403_FORBIDDEN)
